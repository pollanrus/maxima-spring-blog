package life.maxima.spring.dto;

import life.maxima.spring.entity.Post;
import life.maxima.spring.entity.Tag;
import lombok.Getter;
import lombok.Setter;

import java.util.stream.Collectors;

@Setter
@Getter
public class PostDto {

    private String title;
    private String content;
    private String tags;

    public static PostDto fromPost(Post post){
        PostDto postDto = new PostDto();
        postDto.setTitle(post.getTitle());
        postDto.setContent(post.getContent());
        postDto.setTags(post.getTags().stream()
                .map(Tag::getName)
                .collect(Collectors.joining(" ")));

        return postDto;
    }

}
