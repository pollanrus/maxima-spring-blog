package life.maxima.spring.service;

import life.maxima.spring.entity.Comment;
import life.maxima.spring.entity.Post;
import life.maxima.spring.entity.User;
import life.maxima.spring.repository.CommentRepository;
import life.maxima.spring.repository.PostRepository;
import life.maxima.spring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import static life.maxima.spring.utils.SecurityUtils.getCurrentUserDetails;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final CommentRepository commentRepository;

    @Autowired
    public CommentServiceImpl(PostRepository postRepository,
                              UserRepository userRepository,
                              CommentRepository commentRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    @Secured("ROLE_USER")
    public void create(Long postId, String content) {
        Comment comment = new Comment();
        comment.setContent(content);
        comment.setDtCreated(LocalDateTime.now());

        Post post = postRepository.findById(postId).orElseThrow();
        comment.setPost(post);

        User user = userRepository.findByUsername(
                getCurrentUserDetails().getUsername()).orElseThrow();
        comment.setUser(user);
        commentRepository.save(comment);
    }


}
