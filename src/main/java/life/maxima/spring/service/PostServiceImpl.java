package life.maxima.spring.service;

import life.maxima.spring.dto.PostDto;
import life.maxima.spring.entity.Post;
import life.maxima.spring.entity.Tag;
import life.maxima.spring.entity.User;
import life.maxima.spring.repository.PostRepository;
import life.maxima.spring.repository.TagRepository;
import life.maxima.spring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static life.maxima.spring.utils.SecurityUtils.checkAuthorityOnPost;
import static life.maxima.spring.utils.SecurityUtils.getCurrentUserDetails;

@Service
@Transactional
public class PostServiceImpl implements PostService {

    private final TagRepository tagRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;

    @Autowired
    public PostServiceImpl(TagRepository tagRepository,
                           UserRepository userRepository,
                           PostRepository postRepository) {
        this.tagRepository = tagRepository;
        this.userRepository = userRepository;
        this.postRepository = postRepository;
    }

    @Override
    @Secured("ROLE_USER")
    public long create(PostDto postDto) {
        Post post = new Post();
        post.setTitle(postDto.getTitle());
        post.setContent(postDto.getContent());
        post.setTags(parseTags(postDto.getTags()));

        User user = userRepository.findByUsername(
                getCurrentUserDetails().getUsername()).orElseThrow();
        post.setUser(user);
        post.setDtCreated(LocalDateTime.now());

        return postRepository.save(post).getPostId();
    }

    @Override
    public Post findById(long postId) {
        Post post = postRepository.findById(postId).orElseThrow();
        post.getTags().size();
        post.getComments().size();

        return post;
    }

    @Override
//    @PreAuthorize("hasAnyRole('USER')")
    @Secured("ROLE_USER")
    public void update(long postId, PostDto postDto) {
        Post post = postRepository.findById(postId).orElseThrow();
        checkAuthorityOnPost(post);

        if (StringUtils.hasText(postDto.getTitle())) {
            post.setTitle(postDto.getTitle());
        }

        if (StringUtils.hasText(postDto.getContent())) {
            post.setContent(postDto.getContent());
        }

        if (postDto.getTags() != null) {
            Set<Tag> newTags = parseTags(postDto.getTags());
            post.getTags().removeAll(newTags);
            removeUnusedTags(post);

            post.setTags(newTags);
        }

        post.setDtUpdated(LocalDateTime.now());
        postRepository.save(post);
    }

    private void removeUnusedTags(Post post) {
        Set<Tag> unusedTags = post.getTags().stream()
            .filter(t -> t.getPosts().size() == 1)
            .collect(Collectors.toSet());
        if(unusedTags.size() > 0) {
            tagRepository.deleteAll(unusedTags);
        }
    }

    private Set<Tag> parseTags(String tags) {
        if (!StringUtils.hasText(tags)) {
            return new HashSet<>();
        }

        return Arrays.stream(tags.split(" "))
                .filter(StringUtils::hasText)
                .map(tagName -> tagRepository
                        .findByName(tagName)
                        .orElseGet(() -> tagRepository.save(new Tag(tagName))))
                .collect(Collectors.toSet());
    }

}
