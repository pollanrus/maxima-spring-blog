package life.maxima.spring.service;

import life.maxima.spring.dto.PostDto;
import life.maxima.spring.entity.Post;

public interface PostService {
    long create(PostDto postDto);

    Post findById(long postId);

    void update(long postId, PostDto postDto);
}
