package life.maxima.spring.utils;

import life.maxima.spring.entity.Post;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Objects;


public class SecurityUtils {

    private static final String ACCESS_DENIED = "Access Denied";

    public static UserDetails getCurrentUserDetails() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        if (principal instanceof UserDetails userDetails) {
            return userDetails;
        } else {
            throw new AccessDeniedException(ACCESS_DENIED);
        }
    }

    public static void checkAuthorityOnPost(Post post) {
        String username = getCurrentUserDetails().getUsername();

        if (!Objects.equals(username, post.getUser().getUsername())) {
            throw new AccessDeniedException(ACCESS_DENIED);
        }
    }


}
