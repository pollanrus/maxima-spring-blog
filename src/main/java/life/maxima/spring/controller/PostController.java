package life.maxima.spring.controller;

import life.maxima.spring.dto.PostDto;
import life.maxima.spring.entity.Post;
import life.maxima.spring.entity.Tag;
import life.maxima.spring.repository.PostRepository;
import life.maxima.spring.repository.UserRepository;
import life.maxima.spring.service.PostService;
import life.maxima.spring.service.TagService;
import life.maxima.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.stream.Collectors;

import static life.maxima.spring.utils.SecurityUtils.checkAuthorityOnPost;

@Controller
public class PostController {

    private static final Sort SORT_BY_DT = Sort.by("dtCreated").descending();

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final TagService tagService;
    private final PostService postService;

    @Autowired
    public PostController(PostRepository postRepository,
                          UserRepository userRepository,
                          UserService userService,
                          TagService tagService,
                          PostService postService) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.userService = userService;
        this.tagService = tagService;
        this.postService = postService;
    }


    @GetMapping("/posts")
    public String posts(ModelMap model){
        model.put("postList", postRepository.findAll());
        return "posts";
    }


    @GetMapping
    public String blog(ModelMap model,
                       @RequestParam(name = "q", required = false) String query){
        if (StringUtils.hasText(query)) {
            model.put("title", "Search by");
            model.put("subtitle", query.length() > 20 ?
                    query.substring(0, 20) + "..." :
                    query);
            model.put("postList", postRepository.findByContentContainingIgnoreCase(query, SORT_BY_DT));
        } else {
            model.put("title", "Blog");
            model.put("subtitle", "All posts");
            model.put("postList", postRepository.findAll(SORT_BY_DT));
        }

        setCommonParams(model);
        return "blog";
    }

    @GetMapping("/user/{username}")
    public String byUser(ModelMap model, @PathVariable String username){
        model.put("title", "By user");
        model.put("subtitle", username);
//        model.put("postList", postRepository.findByUser_Username(username));
        model.put("postList", userService.findByUsername(username).getPosts());

        setCommonParams(model);
        return "blog";
    }

    @GetMapping("/tag/{tagName}")
    public String byTag(ModelMap model, @PathVariable String tagName){
        model.put("title", "By tag");
        model.put("subtitle", tagName);
        model.put("postList", tagService
                .findByName(tagName)
                .getPosts()
                .stream()
                .sorted(Comparator.comparing(Post::getDtCreated).reversed())
                .collect(Collectors.toList()));

        setCommonParams(model);
        return "blog";
    }

    @GetMapping("/post/new")
    public String postNew(ModelMap model){
        setCommonParams(model);
        return "post-new";
    }

    @PostMapping("/post/new")
    public String postNew(PostDto postDto){
        long postId = postService.create(postDto);
        return "redirect:/post/" + postId;
    }


    @GetMapping("/post/{postId}/edit")
    public String postEdit(@PathVariable long postId, ModelMap model){
        Post post = postService.findById(postId);
        checkAuthorityOnPost(post);

        model.put("postId", postId);
        model.put("post", PostDto.fromPost(post));
        setCommonParams(model);
        return "post-edit";
    }

    @PostMapping("/post/{postId}/edit")
    public String postEdit(@PathVariable long postId,
                           PostDto postDto){
        postService.update(postId, postDto);
        return "redirect:/post/" + postId;
    }



    @GetMapping("/post/{postId}")
    public String post(@PathVariable long postId, ModelMap model){
        Post post = postService.findById(postId);
        model.put("post", post);
        setCommonParams(model);
        return "post";
    }




    private void setCommonParams(ModelMap model) {
        model.put("tags", tagService.findAll()
                .stream()
                .collect(Collectors.toMap(Tag::getName, tag -> tag.getPosts().size())));
        model.put("users", userRepository.findAll(SORT_BY_DT));
    }
}
